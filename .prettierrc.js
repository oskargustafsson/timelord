// prettier.config.js or .prettierrc.js
module.exports = {
  trailingComma: 'es5',
  semi: true,
  singleQuote: true,
  arrowParens: 'always',
  printWidth: 120,
  editorConfig: true,
};
