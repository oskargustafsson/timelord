import Record from 'bff/record';
import View from 'bff/view';

import vent from 'env/vent';

import template from './template.dot';

const bigNumber = 2 * Date.now();

const Filters = Record.withProperties({
  id: { type: RegExp, defaultValue: new RegExp() },
  name: { type: RegExp, defaultValue: new RegExp() },
});

export default View.makeSubclass({
  constructor(tasks, addTaskInputState) {
    this.tasks = tasks;
    this.filters = new Filters();

    this.render();

    this.listenTo(this.tasks, ['change:length', 'item:change'], this.onTasksChanged);
    this.listenTo(this.tasks, 'item:request highlight existing task', this.highlightTask);

    this.listenTo(addTaskInputState, 'change', this.onAddTaskInputStateChanged);
    this.listenTo(this.filters, 'change', this.requestRender);

    this.listenTo('button', 'click', this.onButtonClicked);
    this.listenTo('[data-edit-prop]', 'change', this.onInputChanged);
    this.listenTo('.not-exportable', 'click', this.makeTaskExportable);

    // Defer this listener, to avoid it being triggered on the initial set of tasks
    requestAnimationFrame(() => {
      this.listenTo(tasks, 'item:added', this.onTaskAdded);
    });
  },

  template,

  onTasksChanged() {
    const getSortScore = task => (task.isFavorite ? bigNumber : 0) + task.lastUsed;
    this.tasks.sort((taskA, taskB) => getSortScore(taskB) - getSortScore(taskA));
    this.render();
  },

  onTaskAdded(task) {
    setTimeout(() => {
      this.highlightTask(task, true);
    }, 200); // Wait for filter-miss transition to finish
  },

  onInputChanged(ev) {
    const task = this.getTaskForRowEl(ev.target);
    const propertyName = ev.target.getAttribute('data-edit-prop');
    switch (propertyName) {
      case 'id': {
        const newTaskId = ev.target.value;
        task.id = this.tasks.isValidExportableId(newTaskId)
          ? this.tasks.getNextUniqueId(true, newTaskId)
          : this.tasks.getNextUniqueId(false);
        break;
      }
      case 'name': {
        task.name = ev.target.textContent;
        break;
      }
      default: {
        break;
      }
    }
  },

  onAddTaskInputStateChanged(propName, val) {
    if (propName in this.filters) {
      this.filters[propName] = new RegExp(val, 'i');
    }
  },

  makeTaskExportable(ev) {
    const task = this.getTaskForRowEl(ev.target);
    task.id = this.tasks.getNextUniqueId(true);
  },

  onButtonClicked(ev) {
    // TODO: Fix bug in BFF patchDom and use 'data-id' instead.
    const taskId = ev.target.parentElement.parentElement.getAttribute('data-task-id');
    const actionName = ev.target.getAttribute('action');
    if (actionName) {
      this[actionName](taskId);
    }
  },

  addTaskToTimeline(taskId) {
    const task = this.tasks.find(t => t.id === taskId);
    task.lastUsed = Date.now();
    vent.emit('request add task to timeline', task);
    vent.emit('request clear add task inputs');
  },

  toggleTaskIsFavorite(taskId) {
    const task = this.tasks.find(t => t.id === taskId);
    task.isFavorite = !task.isFavorite;
  },

  toggleTaskIsLocked(taskId) {
    const task = this.tasks.find(t => t.id === taskId);
    task.isLocked = !task.isLocked;
  },

  deleteTask(taskId) {
    this.tasks.filterMut(task => task.id !== taskId);
  },

  highlightTask(task, isLong) {
    // TODO: Come up with nicer way of playing animations
    const animName = isLong ? 'highlight-long-anim-trigger' : 'highlight-anim-trigger';
    const el = this.$(`[data-task-id="${task.id}"]`);
    if (!el) {
      return;
    }
    el.classList.remove(animName);
    this.forceRepaint(el);
    el.classList.add(animName);
    (el.scrollIntoViewIfNeeded || el.scrollIntoView).call(el, true);
  },

  getTaskForRowEl(el) {
    const taskIdString = el.closest('[data-task-id]').getAttribute('data-task-id');
    return this.tasks.find(t => t.id === taskIdString);
  },
});
