import extend from 'bff/extend';
import Record from 'bff/record';
import View from 'bff/view';

import { loadList, saveList, KEYS as localStorageKeys } from 'env/local_storage';
import Task from 'entities/task';

import AddTaskView from './add/view';
import TasksListView from './list/view';
import defaultTasks from './default_tasks.json';
import template from './template.dot';

const State = Record.withProperties({
  prunedTasks: { type: Object, defaultValue: {} },
});

export default View.makeSubclass({
  constructor(tasks) {
    this.tasks = tasks;
    this.state = new State();

    this.render();

    const addTaskView = new AddTaskView(tasks);
    const taskListView = new TasksListView(tasks, addTaskView.inputState);

    this.$('.content-pane').appendChild(addTaskView.el);
    this.$('.content-pane').appendChild(taskListView.el);

    this.listenTo(tasks, 'change', this.requestRender);
    this.listenTo(this.state, 'change', this.requestRender);
    this.listenTo(tasks, ['change:length', 'item:change'], this.storeTasksLocally);
    this.listenTo('button.prune', 'click', this.pruneTasks);
    this.listenTo('button.undoPrune', 'click', this.undoPruneTasks);
    this.listenTo('button.submit', 'click', () => {
      this.state.prunedTasks = {};
    });

    this.loadLocallyStoredTasks();
  },

  template,

  storeTasksLocally() {
    saveList(localStorageKeys.TASKS, this.tasks);
  },

  loadLocallyStoredTasks() {
    const locallyStoredTasksData = loadList(localStorageKeys.TASKS);
    const tasksData = extend(locallyStoredTasksData, defaultTasks, 'useTarget');
    const tasks = tasksData.map(taskData => new Task(taskData));
    this.tasks.pushAll(tasks);
  },

  pruneTasks() {
    const isPrunable = task => !(task.nReferences >= 1 || task.isFavorite || task.isLocked);
    this.state.prunedTasks = this.tasks.filter(task => isPrunable(task));
    this.tasks.filterMut(task => !isPrunable(task));
    setTimeout(() => {
      this.state.prunedTasks = {};
    }, 10000); // clear prune undo state after 10 seconds
  },

  undoPruneTasks() {
    this.tasks.pushAll(this.state.prunedTasks);
    this.state.prunedTasks = {};
  },
});
