import find from 'lodash/find';
import inRange from 'lodash/inRange';

import View from 'bff/view';

import time from 'env/time';
import vent from 'env/vent';
import TimelineItem from 'entities/timeline_item';

import template from './template.dot';
import ItemView from './item/view';

export default View.makeSubclass({
  constructor(items, timelineState) {
    this.items = items;
    this.timelineState = timelineState;

    this.render();

    this.listenTo(items, 'item:added', this.onItemAdded);
    this.listenTo(items, 'item:request delete self', this.onItemRequestDelete);
    this.listenTo(items, 'item:change:startTime', this.onItemStartTimeChanged);
    this.listenTo(items, 'item:change:endTime', this.onItemEndTimeChanged);
    this.listenTo(items, 'item:reorder update', this.onItemReorderDrag);
    this.listenTo(timelineState, 'change', this.requestRender);
    this.listenTo(timelineState, 'change:zoom', this.onZoomChanged);
    this.listenTo(vent, 'app started', this.setInitialScrollPos);
    this.listenTo(vent, 'request add task to timeline', this.onRequestAddTask);
  },

  hours: Array(24).fill(Array(12).fill(true)),

  template,

  emToPx(em) {
    // TODO: move this to BFF.View
    const fontSize = parseFloat(getComputedStyle(this.el).fontSize);
    return em * fontSize;
  },

  onItemAdded(item) {
    const itemView = new ItemView(item, this.items);
    const itemViewEl = itemView.el;
    this.el.insertBefore(itemViewEl, this.$('.modal-layer'));
    (itemViewEl.scrollIntoViewIfNeeded || itemViewEl.scrollIntoView).call(itemViewEl, true);
  },

  onZoomChanged(zoom, prevZoom) {
    const halfHeight = this.el.clientHeight / 2;
    let newScrollTop = this.el.scrollTop + halfHeight;
    newScrollTop *= zoom / prevZoom;
    newScrollTop -= halfHeight;
    this.el.scrollTop = newScrollTop;
  },

  onItemRequestDelete(item) {
    this.items.remove(item);
  },

  setInitialScrollPos() {
    const lastTaskEndTime = this.items.length === 0
      ? new Date().getHours() * 60 + new Date().getMinutes()
      : this.items.last.endTime;
    const scrollTopEm = Math.max(0, (lastTaskEndTime - 3 * 60) * time.minutesToEm);
    const scrollTopPx = this.emToPx(scrollTopEm);
    this.el.scrollTop = scrollTopPx;
  },

  onRequestAddTask(task) {
    const defaultDuration = 15;
    const lastItem = this.items.last;
    const startTime = Math.min(lastItem ? lastItem.endTime : time.getMinutesSinceMidnight(), 24 * 60 - defaultDuration);
    const snappedStartTime = time.snap(startTime);

    if (lastItem && snappedStartTime < lastItem.endTime) {
      return;
    }

    this.items.push(
      new TimelineItem({
        startTime: snappedStartTime,
        endTime: snappedStartTime + defaultDuration,
        task,
      }),
    );
  },

  onItemStartTimeChanged(newStartTime, prevStartTime, item) {
    if (this.ignoreItemTimeChanges) {
      return;
    }
    const itemIndex = this.items.indexOf(item);
    const prevItem = this.items[itemIndex - 1];
    if (!prevItem) {
      return;
    }
    prevItem.endTime = newStartTime;
  },

  onItemEndTimeChanged(newEndTime, prevEndTime, item) {
    if (this.ignoreItemTimeChanges) {
      return;
    }
    const itemIndex = this.items.indexOf(item);
    const nextItem = this.items[itemIndex + 1];
    if (!nextItem) {
      return;
    }
    const nextItemDuration = nextItem.duration;
    nextItem.startTime = newEndTime;
    nextItem.endTime = nextItem.startTime + nextItemDuration;
  },

  onItemReorderDrag(timeOffset, item) {
    const getDirectNeighbours = (items, centerItemIndex) => {
      const beforeIndex = centerItemIndex - 1;
      const afterIndex = centerItemIndex + 1;
      const isValidIndex = value => inRange(value, 0, items.length);

      return {
        before: isValidIndex(beforeIndex) ? items[beforeIndex] : null,
        after: isValidIndex(afterIndex) ? items[afterIndex] : null,
      };
    };

    const candidatesForSwapping = getDirectNeighbours(this.items, this.items.indexOf(item));
    const switchWithItem = find(candidatesForSwapping, (candidate, key) => {
      if (!candidate) return false;
      const itemCenterTime = item.startTime + item.duration / 2 + timeOffset;
      const candidateCenterTime = candidate.startTime + candidate.duration / 2;
      switch (key) {
        case 'before':
          return itemCenterTime < candidateCenterTime;
        case 'after':
          return itemCenterTime > candidateCenterTime;
        default:
          // TODO throw
          return false;
      }
    });
    if (!switchWithItem) return;

    this.ignoreItemTimeChanges = true;
    const prevItemStartTime = item.startTime;
    const firstItem = item.startTime < switchWithItem.startTime ? item : switchWithItem;
    const lastItem = item.startTime < switchWithItem.startTime ? switchWithItem : item;
    const lastItemDuration = lastItem.duration;
    lastItem.startTime = firstItem.startTime;
    lastItem.endTime = lastItem.startTime + lastItemDuration;
    const firstItemDuration = firstItem.duration;
    firstItem.startTime = lastItem.endTime;
    firstItem.endTime = firstItem.startTime + firstItemDuration;
    this.ignoreItemTimeChanges = false;
    this.items.sort((itemA, itemB) => itemA.startTime - itemB.startTime);

    const itemStartTimeDelta = prevItemStartTime - item.startTime;
    item.emit('reordered', itemStartTimeDelta);
    firstItem.emit('moved forward');
  },
});
