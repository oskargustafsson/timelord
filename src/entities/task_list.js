import List from 'bff/list';

import hasJiraPrefix from './util';

const TaskList = List.withProperties({});

TaskList.prototype.isValidExportableId = function(taskId) {
  if (hasJiraPrefix(taskId)) {
    return parseInt(taskId.slice(1), 10) && taskId.length > 1;
  }
  const taskNum = parseInt(taskId, 10);
  return Number.isInteger(taskNum) && taskNum > 0;
};

TaskList.prototype.isTaskIdUnique = function(taskId) {
  return this.every(task => task.id !== String(taskId));
};

TaskList.prototype.getNextUniqueId = function(isExportable, seed) {
  const getNextIdCandidate = id => id + (isExportable ? 1 : -1);
  let nextId = seed || (isExportable ? 0 : -1);
  while (!this.isTaskIdUnique(nextId)) {
    nextId = getNextIdCandidate(nextId);
  }
  return nextId;
};

export default TaskList;
