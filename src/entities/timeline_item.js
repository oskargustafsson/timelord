import Record from 'bff/record';

import Task from './task';

const TimelineItem = Record.withProperties({
  task: {
    type: Task,
    setter: false,
  },
  startTime: {
    type: Number,
    setter(startTime) {
      return Math.floor(startTime);
    },
  },
  endTime: {
    type: Number,
    setter(endTime) {
      return Math.floor(endTime);
    },
  },
  description: {
    type: String,
    defaultValue: '',
  },
  taskId: {
    type: String,
    setter: false,
    getter() {
      return this.task.id;
    },
  },
  duration: {
    type: Number,
    dependencies: ['startTime', 'endTime'],
    setter: false,
    getter() {
      return this.endTime - this.startTime;
    },
  },
});

// eslint-disable-next-line func-names
TimelineItem.prototype.toJSON = function() {
  return {
    startTime: this.startTime,
    endTime: this.endTime,
    taskId: this.taskId,
    description: this.description,
  };
};

export default TimelineItem;
