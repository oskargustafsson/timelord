const snapDuration = 5;

export default {
  minutesToTimeString(minutesSinceMidnight) {
    const hours = Math.floor(minutesSinceMidnight / 60);
    const minutes = Math.floor(minutesSinceMidnight % 60);
    return `${hours}:${minutes < 10 ? '0' : ''}${minutes}`;
  },

  getMinutesSinceMidnight() {
    const now = new Date();
    return now.getHours() * 60 + now.getMinutes();
  },

  snap(minutes) {
    return Math.round(minutes / snapDuration) * snapDuration;
  },

  minutesToEm: 1 / 5,

  emToMinutes: 5 / 1,

  snapDuration,
};
